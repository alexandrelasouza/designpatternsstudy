package com.example.designpatterns.behavioral.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by ale_s on 20/06/2016.
 */
public class EventObserver implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Response received: " + arg);
    }
}
