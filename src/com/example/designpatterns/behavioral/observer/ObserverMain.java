package com.example.designpatterns.behavioral.observer;

/**
 * Created by ale_s on 20/06/2016.
 */
public class ObserverMain {

    public static void main(String[] args) {

        EventSource event = new EventSource();

        event.addObserver(new EventObserver());

        new Thread(event).start();

    }

}
