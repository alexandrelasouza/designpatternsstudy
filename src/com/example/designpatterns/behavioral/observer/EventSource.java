package com.example.designpatterns.behavioral.observer;

import java.util.Observable;
import java.util.Scanner;

/**
 * Created by ale_s on 20/06/2016.
 */
public class EventSource extends Observable implements Runnable {

    private Scanner scanner;

    public EventSource() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("Type a response (CTRL C to stop): ");
            String response = scanner.nextLine();
            setChanged();
            notifyObservers(response);
        }
    }
}
