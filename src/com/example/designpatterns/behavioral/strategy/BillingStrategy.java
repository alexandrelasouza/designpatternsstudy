package com.example.designpatterns.behavioral.strategy;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface BillingStrategy {

    double getActPrice(double rawPrice);

}
