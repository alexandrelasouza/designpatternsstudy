package com.example.designpatterns.behavioral.strategy;

/**
 * Created by ale_s on 20/06/2016.
 */
// Strategy for happy hour (50% discount)
public class HappyHourStrategy implements BillingStrategy {

    @Override
    public double getActPrice(double rawPrice) {
        return rawPrice / 2;
    }
}
