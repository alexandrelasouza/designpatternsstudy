package com.example.designpatterns.behavioral.strategy;

/**
 * Created by ale_s on 20/06/2016.
 */
public class StrategyMain {

    public static void main(String[] args) {
        Customer a = new Customer(new NormalStrategy());

        // Normal billing
        a.add(1.0, 1);

        // Start happy hour
        a.setStrategy(new HappyHourStrategy());
        a.add(1.0, 2);

        // new customer
        Customer b = new Customer(new HappyHourStrategy());
        b.add(0.8, 1);

        // Customer a pays the bill
        a.printBill();


        // end Happy hour
        b.setStrategy(new NormalStrategy());
        b.add(1.3, 2);
        b.add(2.5, 1);
        b.printBill();
    }

}
