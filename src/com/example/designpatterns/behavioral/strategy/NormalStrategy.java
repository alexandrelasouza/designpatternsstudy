package com.example.designpatterns.behavioral.strategy;

/**
 * Created by ale_s on 20/06/2016.
 */
// Normal billing strategy (unchanged price)
public class NormalStrategy implements BillingStrategy {

    @Override
    public double getActPrice(double rawPrice) {
        return rawPrice;
    }
}
