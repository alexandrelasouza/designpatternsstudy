package com.example.designpatterns.behavioral.state;

/**
 * Created by ale_s on 20/06/2016.
 */
public class StateContext {

    private Statelike state;

    public StateContext() {
        setState(new StateLowerCase());
    }

    public void setState(Statelike state) {
        this.state = state;
    }

    public void writeName(final String name) {
        this.state.writeName(this, name);
    }

}
