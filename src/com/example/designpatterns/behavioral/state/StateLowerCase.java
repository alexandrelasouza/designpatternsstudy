package com.example.designpatterns.behavioral.state;

/**
 * Created by ale_s on 20/06/2016.
 */
public class StateLowerCase implements Statelike {

    @Override
    public void writeName(StateContext context, String name) {
        System.out.println(name.toLowerCase());
        context.setState(new StateMultipleUpperCase());
    }
}
