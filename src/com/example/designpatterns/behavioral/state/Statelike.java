package com.example.designpatterns.behavioral.state;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface Statelike {

    void writeName(StateContext context, String name);

}
