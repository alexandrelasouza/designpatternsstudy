package com.example.designpatterns.behavioral.state;

/**
 * Created by ale_s on 20/06/2016.
 */
public class StateMultipleUpperCase implements Statelike {

    private int count = 0;

    @Override
    public void writeName(StateContext context, String name) {
        System.out.println(name.toUpperCase());

        if (++count > 1) {
            context.setState(new StateLowerCase());
        }

    }
}
