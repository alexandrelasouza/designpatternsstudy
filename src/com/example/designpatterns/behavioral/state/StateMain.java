package com.example.designpatterns.behavioral.state;

/**
 * Created by ale_s on 20/06/2016.
 */
public class StateMain {

    public static void main(String[] args) {

        final StateContext context = new StateContext();

        context.writeName("Monday");
        context.writeName("Tuesday");
        context.writeName("Wednesday");
        context.writeName("Thursday");
        context.writeName("Friday");
        context.writeName("Saturday");
        context.writeName("Sunday");

    }

}
