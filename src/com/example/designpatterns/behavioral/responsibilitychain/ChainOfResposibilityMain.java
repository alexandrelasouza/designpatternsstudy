package com.example.designpatterns.behavioral.responsibilitychain;

/**
 * Created by ale_s on 20/06/2016.
 */
public class ChainOfResposibilityMain {

    public static void main(String[] args) {

        PurchasePower manager = new ManagerPurchasePower();
        PurchasePower director = new DirectorPurchasePower();
        PurchasePower vicePresident = new VicePresidentPurchasePower();
        PurchasePower president = new PresidentPurchasePower();

        manager.setSuccessor(director);
        director.setSuccessor(vicePresident);
        vicePresident.setSuccessor(president);

        PurchaseRequest request = new PurchaseRequest(1000, "General");
        manager.processRequest(request);  // manager approves


        request = new PurchaseRequest(2000, "General");
        manager.processRequest(request); // manager approves


        request = new PurchaseRequest(6000, "General");
        manager.processRequest(request); // director approves


        request = new PurchaseRequest(7000, "General");
        manager.processRequest(request); // director approves

        request = new PurchaseRequest(12000, "General");
        manager.processRequest(request); // vice president approves

        request = new PurchaseRequest(15000, "General");
        manager.processRequest(request); // vice president approves

        request = new PurchaseRequest(18000, "General");
        manager.processRequest(request); // vice president approves

        request = new PurchaseRequest(21000, "General");
        manager.processRequest(request); // president approves


    }

}
