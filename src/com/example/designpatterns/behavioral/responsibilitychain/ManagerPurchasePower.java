package com.example.designpatterns.behavioral.responsibilitychain;

/**
 * Created by ale_s on 20/06/2016.
 */
public class ManagerPurchasePower extends PurchasePower {

    @Override
    protected double getAllowable() {
        return BASE * 10;
    }

    @Override
    protected String getRole() {
        return "Manager";
    }
}
