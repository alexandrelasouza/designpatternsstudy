package com.example.designpatterns.behavioral.responsibilitychain;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class PurchasePower {

    protected static final double BASE = 500;
    protected PurchasePower successor;

    abstract protected double getAllowable();
    abstract protected String getRole();

    public void setSuccessor(PurchasePower successor) {
        this.successor = successor;
    }

    public void processRequest(PurchaseRequest request) {
        if (request.getAmount() < getAllowable()) {
            System.out.println(this.getRole() + " will approve $" + request.getAmount());
        } else if (successor != null){
            successor.processRequest(request);
        }
    }

}
