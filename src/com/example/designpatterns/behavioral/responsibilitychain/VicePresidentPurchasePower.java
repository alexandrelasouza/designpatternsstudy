package com.example.designpatterns.behavioral.responsibilitychain;

/**
 * Created by ale_s on 20/06/2016.
 */
public class VicePresidentPurchasePower extends PurchasePower {

    @Override
    protected double getAllowable() {
        return BASE * 40;
    }

    @Override
    protected String getRole() {
        return "Vice President";
    }
}
