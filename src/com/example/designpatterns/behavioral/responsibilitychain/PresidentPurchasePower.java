package com.example.designpatterns.behavioral.responsibilitychain;

/**
 * Created by ale_s on 20/06/2016.
 */
public class PresidentPurchasePower extends PurchasePower {

    @Override
    protected double getAllowable() {
        return BASE * 60;
    }

    @Override
    protected String getRole() {
        return "President";
    }

}
