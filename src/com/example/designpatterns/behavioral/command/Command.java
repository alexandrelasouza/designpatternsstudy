package com.example.designpatterns.behavioral.command;

/**
 * Created by ale_s on 20/06/2016.
 */
// Command interface
public interface Command {

    void execute();

}
