package com.example.designpatterns.behavioral.command;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale_s on 20/06/2016.
 */
// This is the invoker class
public class Switch {

    private List<Command> commandHistory = new ArrayList<>();

    public void storeAndExecute(Command command) {
        commandHistory.add(command);
        command.execute();
    }

}
