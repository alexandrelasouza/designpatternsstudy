package com.example.designpatterns.behavioral.command;

/**
 * Created by ale_s on 20/06/2016.
 */
public class CommandMain {

    public static void main(String[] args) {

        Light lamp = new Light();
        Command flipUpCmd = new FlipUpCommand(lamp);
        Command flipDownCmd = new FlipDownCommand(lamp);

        Switch lampSwitch = new Switch();

        lampSwitch.storeAndExecute(flipUpCmd);
        lampSwitch.storeAndExecute(flipDownCmd);

        System.out.println("\n\n");


        lampSwitch.storeAndExecute(flipUpCmd);
        lampSwitch.storeAndExecute(flipDownCmd);

        System.out.println("\n\n");

        lampSwitch.storeAndExecute(flipUpCmd);
        lampSwitch.storeAndExecute(flipDownCmd);

        System.out.println("\n\n");

        lampSwitch.storeAndExecute(flipUpCmd);
        lampSwitch.storeAndExecute(flipDownCmd);

        System.out.println("\n\n");

        lampSwitch.storeAndExecute(flipUpCmd);
        lampSwitch.storeAndExecute(flipDownCmd);

    }

}
