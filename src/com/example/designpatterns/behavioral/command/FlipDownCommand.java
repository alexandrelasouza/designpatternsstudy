package com.example.designpatterns.behavioral.command;

/**
 * Created by ale_s on 20/06/2016.
 */
public class FlipDownCommand implements Command {

    private Light light;

    public FlipDownCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.turnOff();
    }
}
