package com.example.designpatterns.behavioral.command;

/**
 * Created by ale_s on 20/06/2016.
 */
// This is the receiver class
public class Light {

    public void turnOn() {
        System.out.println("Turning light on");
    }

    public void turnOff() {
        System.out.println("Turning light off");
    }

}
