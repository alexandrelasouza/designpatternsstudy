package com.example.designpatterns.behavioral.mediator;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface ChatMediator {

    void sendMessage(User user, String message);

    void addUser(User user);

}
