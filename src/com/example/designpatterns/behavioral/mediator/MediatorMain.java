package com.example.designpatterns.behavioral.mediator;

/**
 * Created by ale_s on 20/06/2016.
 */
public class MediatorMain {

    public static void main(String[] args) {

        ChatMediator mediator = new ChatMediatorImpl();
        User user1 = new CommonUser("Alexandre", mediator);
        User user2 = new CommonUser("Maria", mediator);
        User user3 = new CommonUser("Jose", mediator);
        User user4 = new CommonUser("Rafa", mediator);

        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);
        mediator.addUser(user4);


        user1.send("Hi all");

        System.out.println("\n");

        user3.send("Hello modafoca");

    }

}
