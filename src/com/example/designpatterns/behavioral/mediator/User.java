package com.example.designpatterns.behavioral.mediator;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class User {

    private String name;
    protected ChatMediator chatMediator;

    public User(String name, ChatMediator chatMediator) {
        this.name = name;
        this.chatMediator = chatMediator;
    }

    public String getName() {
        return name;
    }

    public abstract void send(String msg);
    public abstract void receive(String msg);

}
