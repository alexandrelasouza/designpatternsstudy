package com.example.designpatterns.behavioral.mediator;

/**
 * Created by ale_s on 20/06/2016.
 */
public class CommonUser extends User {

    public CommonUser(String name, ChatMediator chatMediator) {
        super(name, chatMediator);
    }

    @Override
    public void send(String msg) {
        System.out.println(getName() + ": Sending message = " + msg);
        chatMediator.sendMessage(this, msg);
    }

    @Override
    public void receive(String msg) {
        System.out.println(getName() + ": Message received = " + msg);
    }
}
