package com.example.designpatterns.behavioral.templatemethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Concrete2 extends AbstractClass {

    @Override
    public void primitiveOperation1() {
        System.out.println("Concrete2.primitiveOperation1() called");
    }

    @Override
    public void primitiveOperation2() {
        System.out.println("Concrete2.primitiveOperation2() called");
    }
}
