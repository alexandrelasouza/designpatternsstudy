package com.example.designpatterns.behavioral.templatemethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Concrete1 extends AbstractClass {

    @Override
    public void primitiveOperation1() {
        System.out.println("Concrete1.primitiveOperation1() called");
    }

    @Override
    public void primitiveOperation2() {
        System.out.println("Concrete1.primitiveOperation2() called");
    }
}
