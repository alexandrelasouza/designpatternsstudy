package com.example.designpatterns.behavioral.templatemethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class AbstractClass {

    public final void templateMethod() {
        primitiveOperation1();
        primitiveOperation2();
    }

    public abstract void primitiveOperation1();
    public abstract void primitiveOperation2();

}
