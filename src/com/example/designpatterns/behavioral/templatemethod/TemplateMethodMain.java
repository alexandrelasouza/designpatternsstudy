package com.example.designpatterns.behavioral.templatemethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class TemplateMethodMain {

    public static void main(String[] args) {

        AbstractClass c1 = new Concrete1();
        AbstractClass c2 = new Concrete2();

        c1.templateMethod();
        c2.templateMethod();

    }

}
