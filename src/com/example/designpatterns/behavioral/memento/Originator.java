package com.example.designpatterns.behavioral.memento;

/**
 * The originator is an object with a state
 * This state may be saved in a Memento for further restoration if needed
 */
public class Originator {

    private String state;

    public void setState(String state) {
        System.out.println("Originator -> setting state to: " + state);
        this.state = state;
    }

    public Memento saveToMemento() {
        System.out.println("Originator -> Saving state to memento: " + state);
        return new Memento(this.state);
    }

    public void restoreFromMemento(Memento memento) {
        this.state = memento.getSavedState();
        System.out.println("Originator -> state after memento restoration: " + state);
    }


    public static class Memento {

        private final String savedState;

        public Memento(String stateToSave) {
            this.savedState = stateToSave;
        }

        public String getSavedState() {
            return savedState;
        }
    }

}
