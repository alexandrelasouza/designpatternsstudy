package com.example.designpatterns.behavioral.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale_s on 20/06/2016.
 */
public class MementoMain {

    public static void main(String[] args) {

        List<Originator.Memento> savedStates = new ArrayList<>();

        Originator originator = new Originator();
        originator.setState("State1");
        originator.setState("State2");
        savedStates.add(originator.saveToMemento());
        originator.setState("State3");

        // We can request multiple mementos, and choose which one to roll back to.
        savedStates.add(originator.saveToMemento());
        originator.setState("State4");

        originator.restoreFromMemento(savedStates.get(0));

    }

}
