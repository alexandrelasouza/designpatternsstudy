package com.example.designpatterns.behavioral.visitor;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface CarElementVisitor {

    void visit(Wheel wheel);
    void visit(Engine engine);
    void visit(Body body);
    void visit(Car car);

}
