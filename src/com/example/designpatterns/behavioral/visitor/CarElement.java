package com.example.designpatterns.behavioral.visitor;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface CarElement {

    void accept(CarElementVisitor visitor);

}
