package com.example.designpatterns.behavioral.visitor;

/**
 * Created by ale_s on 20/06/2016.
 */
public class VisitorMain {

    public static void main(String[] args) {

        Car car = new Car();
        car.accept(new CarElementPrintVisitor());
        car.accept(new CarElementDoVisitor());

    }

}
