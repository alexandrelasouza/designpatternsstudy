package com.example.designpatterns.behavioral.visitor;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Body implements CarElement {

    @Override
    public void accept(CarElementVisitor visitor) {
        visitor.visit(this);
    }
}
