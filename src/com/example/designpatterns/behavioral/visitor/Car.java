package com.example.designpatterns.behavioral.visitor;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Car implements CarElement {

    private CarElement[] elements;

    public Car() {
        this.elements = new CarElement[] {
                new Wheel("Front left"),
                new Wheel("Front right"),
                new Wheel("Back left"),
                new Wheel("Back right"),
                new Body(), new Engine()
        };
    }

    @Override
    public void accept(CarElementVisitor visitor) {

        for (CarElement element : elements) {
            element.accept(visitor);
        }

        visitor.visit(this);
    }
}
