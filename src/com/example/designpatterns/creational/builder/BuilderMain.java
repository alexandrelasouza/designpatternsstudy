package com.example.designpatterns.creational.builder;

import java.awt.*;

/**
 * Created by ale_s on 20/06/2016.
 */
public class BuilderMain {

    public static void main(String[] args) {
        StreetMap map = new StreetMap.Builder(50, 50, 100, 100)
                .landColor(Color.GRAY)
                .waterColor(Color.BLUE.brighter())
                .highTrafficColor(Color.RED.darker())
                .mediumTrafficColor(Color.ORANGE.darker())
                .lowTrafficColor(Color.PINK.darker())
                .build();

        StreetMap map2 = new StreetMap.Builder(50, 65, 100, 110)
                .build();


        System.out.println("Custom: Map 1");
        System.out.println("Source X: " + map.getSource().getX() + ". Source Y: " + map.getSource().getY());
        System.out.println("Dest X: " + map.getDestination().getX() + ". Dest Y: " + map.getDestination().getY());
        System.out.println("Land color: " + map.getLandColor());
        System.out.println("Water color: " + map.getWaterColor());
        System.out.println("High traffic: " + map.getHighTrafficColor());
        System.out.println("Medium traffic: " + map.getMediumTrafficColor());
        System.out.println("Low traffic: " + map.getLowTrafficColor());

        System.out.println("\n\n");

        System.out.println("Default Map 2");
        System.out.println("Source X: " + map2.getSource().getX() + ". Source Y: " + map2.getSource().getY());
        System.out.println("Dest X: " + map2.getDestination().getX() + ". Dest Y: " + map2.getDestination().getY());
        System.out.println("Land color: " + map2.getLandColor());
        System.out.println("Water color: " + map2.getWaterColor());
        System.out.println("High traffic: " + map2.getHighTrafficColor());
        System.out.println("Medium traffic: " + map2.getMediumTrafficColor());
        System.out.println("Low traffic: " + map2.getLowTrafficColor());

    }

}
