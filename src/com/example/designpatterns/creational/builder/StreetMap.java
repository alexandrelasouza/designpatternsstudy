package com.example.designpatterns.creational.builder;

import java.awt.*;

/**
 * Created by ale_s on 20/06/2016.
 */
public class StreetMap {

    private final Point source;
    private final Point destination;

    private final Color waterColor;
    private final Color landColor;
    private final Color highTrafficColor;
    private final Color mediumTrafficColor;
    private final Color lowTrafficColor;


    private StreetMap(Builder builder) {
        this.source = builder.source;
        this.destination = builder.destination;

        this.waterColor = builder.waterColor;
        this.landColor = builder.landColor;
        this.highTrafficColor = builder.highTrafficColor;
        this.mediumTrafficColor = builder.mediumTrafficColor;
        this.lowTrafficColor = builder.lowTrafficColor;
    }


    public Point getSource() {
        return source;
    }

    public Point getDestination() {
        return destination;
    }

    public Color getWaterColor() {
        return waterColor;
    }

    public Color getLandColor() {
        return landColor;
    }

    public Color getHighTrafficColor() {
        return highTrafficColor;
    }

    public Color getMediumTrafficColor() {
        return mediumTrafficColor;
    }

    public Color getLowTrafficColor() {
        return lowTrafficColor;
    }

    public static class Builder {

        private final Point source;
        private final Point destination;

        // default values
        private Color waterColor = Color.BLUE;
        private Color landColor = new Color(30, 30, 30);
        private Color highTrafficColor = Color.RED;
        private Color mediumTrafficColor = Color.ORANGE;
        private Color lowTrafficColor = Color.YELLOW;

        public Builder(int xSource, int ySource, int xDest, int yDest) {
            this.source = new Point(xSource, ySource);
            this.destination = new Point(xDest, yDest);
        }

        public Builder waterColor(Color color) {
            this.waterColor = color;
            return this;
        }

        public Builder landColor(Color color) {
            this.landColor = color;
            return this;
        }

        public Builder highTrafficColor(Color color) {
            this.highTrafficColor = color;
            return this;
        }

        public Builder mediumTrafficColor(Color color) {
            this.mediumTrafficColor = color;
            return this;
        }

        public Builder lowTrafficColor(Color color) {
            this.lowTrafficColor = color;
            return this;
        }

        public StreetMap build() {
            return new StreetMap(this);
        }

    }

}
