package com.example.designpatterns.creational.builder;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Point {

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
