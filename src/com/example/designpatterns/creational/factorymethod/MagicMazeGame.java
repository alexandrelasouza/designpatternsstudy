package com.example.designpatterns.creational.factorymethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class MagicMazeGame extends MazeGame {

    public MagicMazeGame() {
        super();
    }

    @Override
    protected Room makeRoom() {
        System.out.println("Creating magic room");
        return new MagicRoom(100);
    }
}
