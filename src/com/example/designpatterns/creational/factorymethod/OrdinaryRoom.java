package com.example.designpatterns.creational.factorymethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class OrdinaryRoom extends Room {

    public OrdinaryRoom(int area) {
        super(area);
    }

    @Override
    public void connect(Room connect) {
        System.out.println("Connecting ordinary room to an adjacent room of area: " + getArea());
    }
}
