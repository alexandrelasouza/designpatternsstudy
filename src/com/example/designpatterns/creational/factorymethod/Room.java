package com.example.designpatterns.creational.factorymethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class Room {

    private int area;

    public Room(int area) {
        this.area = area;
    }

    public abstract void connect(Room connect);

    public int getArea() {
        return area;
    }
}
