package com.example.designpatterns.creational.factorymethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class MagicRoom extends Room {

    public MagicRoom(int area) {
        super(area);
    }

    @Override
    public void connect(Room connect) {
        System.out.println("Connecting a room that may be on other part of this maze with area: " + getArea());
    }

}
