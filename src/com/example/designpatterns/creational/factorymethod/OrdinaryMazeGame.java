package com.example.designpatterns.creational.factorymethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class OrdinaryMazeGame extends MazeGame {

    public OrdinaryMazeGame() {
        super();
    }

    @Override
    protected Room makeRoom() {
        System.out.println("Creating ordinary room");
        return new OrdinaryRoom(40);
    }
}
