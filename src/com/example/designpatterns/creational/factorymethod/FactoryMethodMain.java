package com.example.designpatterns.creational.factorymethod;

/**
 * Created by ale_s on 20/06/2016.
 */
public class FactoryMethodMain {

    public static void main(String[] args) {
        MazeGame game1 = new OrdinaryMazeGame();
        MazeGame game2 = new MagicMazeGame();
    }

}
