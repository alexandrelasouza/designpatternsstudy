package com.example.designpatterns.creational.factorymethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class MazeGame {

    private final List<Room> rooms = new ArrayList<>();

    public MazeGame() {

        Room room1 = makeRoom();
        Room room2 = makeRoom();

        room1.connect(room2);

        rooms.add(room1);
        rooms.add(room2);
    }

    // THIS IS THE FACTORY METHOD
    protected abstract Room makeRoom();
}
