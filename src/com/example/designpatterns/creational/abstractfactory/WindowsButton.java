package com.example.designpatterns.creational.abstractfactory;

/**
 * Created by ale_s on 20/06/2016.
 */
public class WindowsButton implements Widget {

    @Override
    public void draw() {
        System.out.println("Drawing Windows button");
    }
}
