package com.example.designpatterns.creational.abstractfactory;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface Widget {

    void draw();

}
