package com.example.designpatterns.creational.abstractfactory;

/**
 * Created by ale_s on 20/06/2016.
 */
public class OSXWidgetFactory extends WidgetFactory {

    @Override
    public Widget createWidget() {
        return new OSXButton();
    }
}
