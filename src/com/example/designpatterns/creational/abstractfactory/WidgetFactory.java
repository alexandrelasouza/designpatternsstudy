package com.example.designpatterns.creational.abstractfactory;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class WidgetFactory {

    public static WidgetFactory getFactory(String os) {

        boolean isWindows = os.contains("Windows");

        if (isWindows) {
            return new WindowsWidgetFactory();
        } else {
            return new OSXWidgetFactory();
        }

    }


    public abstract Widget createWidget();

}
