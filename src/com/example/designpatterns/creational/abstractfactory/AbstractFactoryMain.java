package com.example.designpatterns.creational.abstractfactory;

/**
 * Created by ale_s on 20/06/2016.
 */
public class AbstractFactoryMain {

    public static void main(String[] args) {
        String os = System.getProperty("os.name");

        WidgetFactory.getFactory(os).createWidget().draw();

        WidgetFactory.getFactory("OSX").createWidget().draw();
    }

}
