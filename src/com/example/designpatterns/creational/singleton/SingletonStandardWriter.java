package com.example.designpatterns.creational.singleton;

/**
 * Created by ale_s on 20/06/2016.
 */
public class SingletonStandardWriter {

    private static SingletonStandardWriter sInstance;

    private SingletonStandardWriter() {}

    public static SingletonStandardWriter getInstance() {
        if (sInstance == null) {
            sInstance = new SingletonStandardWriter();
        }

        return sInstance;

    }

    public void writeMessage(String message) {
        System.out.println(message);
    }


}
