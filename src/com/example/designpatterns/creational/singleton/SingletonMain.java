package com.example.designpatterns.creational.singleton;

/**
 * Created by ale_s on 20/06/2016.
 */
public class SingletonMain {

    public static void main(String[] args) {
        SingletonStandardWriter writer = SingletonStandardWriter.getInstance();
        writer.writeMessage("Hello Singleton pattern :D");
    }

}
