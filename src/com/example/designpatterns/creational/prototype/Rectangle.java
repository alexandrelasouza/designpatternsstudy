package com.example.designpatterns.creational.prototype;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Rectangle extends Shape {

    public Rectangle() {
        super("2", "Rectangle");
    }

    @Override
    public void draw() {
        System.out.println("Drawing a " + getType());
    }
}
