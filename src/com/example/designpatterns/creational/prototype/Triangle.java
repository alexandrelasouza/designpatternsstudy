package com.example.designpatterns.creational.prototype;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Triangle extends Shape {

    public Triangle() {
        super("3", "Triangle");
    }

    @Override
    public void draw() {
        System.out.println("Drawing a " + getType());
    }
}
