package com.example.designpatterns.creational.prototype;

/**
 * Created by ale_s on 20/06/2016.
 */
public class PrototypeMain {

    public static void main(String[] args) {
        Shape shape = ShapeCache.getShape("1");
        Shape shape2 = ShapeCache.getShape("2");
        Shape shape3 = ShapeCache.getShape("3");


        shape.draw();
        shape2.draw();
        shape3.draw();

    }

}
