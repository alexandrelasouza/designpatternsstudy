package com.example.designpatterns.creational.prototype;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Circle extends Shape {

    public Circle() {
        super("1", "Circle");
    }

    @Override
    public void draw() {
        System.out.println("Drawing a " + getType());
    }
}
