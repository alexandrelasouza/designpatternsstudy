package com.example.designpatterns.creational.prototype;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ale_s on 20/06/2016.
 */
public class ShapeCache {

    private static Map<String, Shape> shapeCache = new HashMap<>();

    // prevents instantiation
    private ShapeCache() {}

    public static Shape getShape(String type) {
        loadCache();
        Shape cachedShape = shapeCache.get(type);
        return (Shape) cachedShape.clone(); // clone prototype instances
    }

    private static void loadCache() {

        if (shapeCache.isEmpty()) {
            Circle circle = new Circle(); // create prototype instances
            Triangle triangle = new Triangle();
            Rectangle rectangle = new Rectangle();

            shapeCache.put(circle.getId(), circle);
            shapeCache.put(triangle.getId(), triangle);
            shapeCache.put(rectangle.getId(), rectangle);
        }

    }

}
