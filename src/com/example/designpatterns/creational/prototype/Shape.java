package com.example.designpatterns.creational.prototype;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class Shape implements Cloneable {

    private String id;
    private String type;

    public Shape(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public abstract void draw();

    public Object clone() {

        Object clone = null;

        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return clone;

    }

}
