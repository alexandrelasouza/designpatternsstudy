package com.example.designpatterns.structural.composite;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Ellipse implements Graphic {

    private int number;

    public Ellipse(int number) {
        this.number = number;
    }

    @Override
    public void draw() {
        System.out.println("Drawing Ellipse " + number);
    }
}
