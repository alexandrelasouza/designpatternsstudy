package com.example.designpatterns.structural.composite;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface Graphic {

    void draw();
    
}
