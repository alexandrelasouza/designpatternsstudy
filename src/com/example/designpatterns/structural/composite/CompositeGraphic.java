package com.example.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale_s on 20/06/2016.
 */
// The composite pattern describes that a group of objects is to be treated in the same way as a single instance of an object.
// This is why the CompositeGraphic class implements the same interface as its children
public class CompositeGraphic implements Graphic {

    private List<Graphic> graphics;

    public CompositeGraphic() {
        graphics = new ArrayList<>();
    }

    @Override
    public void draw() {
        for (Graphic g : graphics) {
            g.draw();
        }
    }

    public void add(Graphic g) {
        graphics.add(g);
    }

    public void remove(Graphic g) {
        graphics.add(g);
    }

}
