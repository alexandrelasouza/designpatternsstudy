package com.example.designpatterns.structural.decorator;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface Coffee {

    double getCost();
    String getIngredients();

}
