package com.example.designpatterns.structural.decorator;

/**
 * Created by ale_s on 20/06/2016.
 */
// This decorator mixes Coffee with Milk
public class Milk extends CoffeeDecorator {

    public Milk(Coffee coffee) {
        super(coffee);
    }

    @Override
    public String getIngredients() {
        return super.getIngredients() + ", milk";
    }

    @Override
    public double getCost() {
        return super.getCost() + 0.5;
    }

}
