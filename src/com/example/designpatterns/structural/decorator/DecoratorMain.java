package com.example.designpatterns.structural.decorator;

/**
 * Created by ale_s on 20/06/2016.
 */
public class DecoratorMain {

    public static void main(String[] args) {

        Coffee coffee = new BlackCoffee();

        System.out.println("Cost and ingredients: " + coffee.getCost() + " | " + coffee.getIngredients());

        coffee = new Milk(coffee);

        System.out.println("Cost and ingredients: " + coffee.getCost() + " | " + coffee.getIngredients());

        coffee = new Sprinkles(coffee);

        System.out.println("Cost and ingredients: " + coffee.getCost() + " | " + coffee.getIngredients());


    }

}
