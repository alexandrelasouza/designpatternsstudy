package com.example.designpatterns.structural.decorator;

/**
 * Created by ale_s on 20/06/2016.
 */
public class BlackCoffee implements Coffee {

    @Override
    public double getCost() {
        return 1;
    }

    @Override
    public String getIngredients() {
        return "Black coffee";
    }
}
