package com.example.designpatterns.structural.decorator;

/**
 * Created by ale_s on 20/06/2016.
 */
// This decorator mixes sprinkles onto coffee.
public class Sprinkles extends CoffeeDecorator {

    public Sprinkles(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double getCost() {
        return super.getCost() + 0.2;
    }

    @Override
    public String getIngredients() {
        return super.getIngredients() + ", sprinkles";
    }
}
