package com.example.designpatterns.structural.adapter;

/**
 * Created by ale_s on 20/06/2016.
 */
public class VlcPlayer implements AdvancedMediaPlayer {

    @Override
    public void play(String fileName, int encodingType) {
        System.out.println("Playing VLC file: " + fileName + " with encoding type: " + encodingType);
    }
}
