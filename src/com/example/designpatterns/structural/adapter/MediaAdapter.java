package com.example.designpatterns.structural.adapter;

/**
 * Created by ale_s on 20/06/2016.
 */
public class MediaAdapter implements MediaPlayer {

    private AdvancedMediaPlayer adaptee;



    public MediaAdapter(String audioType) {

        if (audioType.equalsIgnoreCase("vlc")) {
            adaptee = new VlcPlayer();
        } else {
            adaptee = new MP4Player();
        }

    }

    @Override
    public void play(String audioType, String fileName) {
        int encoding = audioType.equals("vlc") ? 1 : 2;
        adaptee.play(fileName, encoding);
    }
}
