package com.example.designpatterns.structural.adapter;

/**
 * Created by ale_s on 20/06/2016.
 */
public class AudioPlayer implements MediaPlayer {

    @Override
    public void play(String audioType, String fileName) {

        if (audioType.equalsIgnoreCase("vlc") || audioType.equals("mp4")) {
            MediaAdapter adapter = new MediaAdapter(audioType);
            adapter.play(audioType, fileName);
        } else if (audioType.equalsIgnoreCase("mp3")){
            System.out.println("Playing default MP3 audio format");
        } else {
            System.out.println("Invalid media type: " + audioType);
        }
    }
}
