package com.example.designpatterns.structural.adapter;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface MediaPlayer {

    void play(String audioType, String fileName);

}
