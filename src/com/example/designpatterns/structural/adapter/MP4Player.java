package com.example.designpatterns.structural.adapter;

/**
 * Created by ale_s on 20/06/2016.
 */
public class MP4Player implements AdvancedMediaPlayer {

    @Override
    public void play(String fileName, int encodingType) {
        System.out.println("Playing MP4 file: " + fileName + " with encoding type: " + encodingType);
    }
}
