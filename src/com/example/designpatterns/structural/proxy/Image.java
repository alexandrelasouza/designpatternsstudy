package com.example.designpatterns.structural.proxy;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface Image {

    void show();

}
