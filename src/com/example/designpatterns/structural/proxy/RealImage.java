package com.example.designpatterns.structural.proxy;

/**
 * Created by ale_s on 20/06/2016.
 */
// ON SYSTEM B
public class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
        this.loadImageFromDisc();
    }

    private void loadImageFromDisc() {
        System.out.println("Loading image from disc");
    }

    @Override
    public void show() {
        System.out.println("Showing " + fileName);
    }
}
