package com.example.designpatterns.structural.proxy;

/**
 * Created by ale_s on 20/06/2016.
 */
public class ProxyMain {

    public static void main(String[] args) {

        Image i1 = new ImageProxy("ResOi_10MB_Foto1");
        Image i2 = new ImageProxy("ResOi_10MB_Foto2");

        i1.show(); // needs loading
        i1.show(); // no need for loading
        i2.show(); // needs loading
        i2.show(); // no need for loading
        i2.show(); // no need for loading
        i1.show(); // no need for loading

    }

}
