package com.example.designpatterns.structural.proxy;

/**
 * Created by ale_s on 20/06/2016.
 */
// FOR USE ON SYSTEM A
public class ImageProxy implements Image {

    private String fileName;
    private Image proxiedImage;

    public ImageProxy(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void show() {
        if (proxiedImage == null) {
            proxiedImage = new RealImage(fileName); // Ideally this would be a lookup for a remote interface
        }

        proxiedImage.show();
    }
}
