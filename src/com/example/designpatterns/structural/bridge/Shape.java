package com.example.designpatterns.structural.bridge;

/**
 * Created by ale_s on 20/06/2016.
 */
public abstract class Shape {

    protected Drawable drawable;

    protected Shape(Drawable drawable) {
        this.drawable = drawable;
    }

    public abstract void draw();

}
