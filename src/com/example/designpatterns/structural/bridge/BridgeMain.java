package com.example.designpatterns.structural.bridge;

/**
 * Created by ale_s on 20/06/2016.
 */
public class BridgeMain {

    public static void main(String[] args) {

        Shape redCircle = new Circle(10, 10, 10, new RedCircle());
        Shape greenCircle = new Circle(30, 100, 110, new GreenCircle());

        redCircle.draw();
        greenCircle.draw();

    }

}
