package com.example.designpatterns.structural.bridge;

/**
 * Created by ale_s on 20/06/2016.
 */
public class GreenCircle implements Drawable {

    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: green, radius: " + radius + ", x: " + x + ", " + y + "]");
    }
}
