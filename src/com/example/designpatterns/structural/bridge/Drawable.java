package com.example.designpatterns.structural.bridge;

/**
 * Created by ale_s on 20/06/2016.
 */
public interface Drawable {

    void drawCircle(int radius, int x, int y);

}
