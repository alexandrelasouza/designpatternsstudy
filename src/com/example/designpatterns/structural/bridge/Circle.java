package com.example.designpatterns.structural.bridge;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Circle extends Shape {

    private int x;
    private int y;
    private int radius;

    public Circle(int x, int y, int radius, Drawable drawable) {
        super(drawable);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    @Override
    public void draw() {
        drawable.drawCircle(radius, x, y);
    }
}
