package com.example.designpatterns.structural.flyweight;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Order {

    private final int tableNumber;
    private final CoffeeFlavour flavour;

    public Order(int tableNumber, CoffeeFlavour flavour) {
        this.tableNumber = tableNumber;
        this.flavour = flavour;
    }

    public void serve() {
        System.out.println("Serving " + flavour + " to table " + tableNumber);
    }

}
