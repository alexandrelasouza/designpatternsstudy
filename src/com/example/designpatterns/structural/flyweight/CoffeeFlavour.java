package com.example.designpatterns.structural.flyweight;

/**
 * Created by ale_s on 20/06/2016.
 */
// THIS WILL BE THE FLYWEIGHTS
public class CoffeeFlavour {

    private final String name;

    public CoffeeFlavour(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
