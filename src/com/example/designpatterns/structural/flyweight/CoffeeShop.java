package com.example.designpatterns.structural.flyweight;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale_s on 20/06/2016.
 */
public class CoffeeShop {

    private final List<Order> orders = new ArrayList<>(); // for thread-safety use Vector<>
    private final Menu menu = new Menu();

    public void takeOrder(String flavourName, int table) {
        CoffeeFlavour flavour = menu.lookup(flavourName);
        orders.add(new Order(table, flavour));
    }

    public void service() {
        for (Order o : orders) {
            o.serve();
        }
    }

    public void report() {
        System.out.println("\nTotal CoffeeFlavour objects made: " + menu.totalCoffeeFlavoursMade());
    }

}
