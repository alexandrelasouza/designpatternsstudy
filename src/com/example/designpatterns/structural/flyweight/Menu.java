package com.example.designpatterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ale_s on 20/06/2016.
 */
public class Menu {

    private Map<String, CoffeeFlavour> flavours = new HashMap<>(); // for thread-safety use ConcurrentHashMap

    public CoffeeFlavour lookup(String flavourName) {

        if (!flavours.containsKey(flavourName)) {
            flavours.put(flavourName, new CoffeeFlavour(flavourName));
        }

        return flavours.get(flavourName);

    }

    public int totalCoffeeFlavoursMade() {
        return flavours.size();
    }

}
